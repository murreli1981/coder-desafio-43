export default interface IRepository {
  getAll(): Promise<any>;
  getById(id: String): Promise<any>;
  getByFilter(filter: {}): Promise<any>;
  create(payload: any): Promise<any>;
  update(id: String, payload: any): Promise<any>;
  delete(id: String): Promise<any>;
}
