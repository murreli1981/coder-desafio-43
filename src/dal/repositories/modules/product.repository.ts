import BaseRepository from "../base.repository";

export default class ProductRepository extends BaseRepository {
  constructor(ProductModel: any) {
    super(ProductModel);
  }
}
