import UserRepository from "./modules/message.repository";
import MessageRepository from "./modules/message.repository";
import ProductRepository from "./modules/product.repository";

import models from "../models";

export default {
  userRepository: new UserRepository(models.UserModel),
  messageRepository: new MessageRepository(models.MessageModel),
  productRepository: new ProductRepository(models.ProductModel),
};
