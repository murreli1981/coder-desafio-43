import MessageModel from "./message";
import ProductModel from "./product";
import UserModel from "./user";

export default {
  UserModel,
  ProductModel,
  MessageModel,
};
