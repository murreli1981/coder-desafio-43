import { Request, Response } from "express";
import IController from "../interfaces/Controllers/IController";
import IService from "../interfaces/Services/IService";

export default class BaseController implements IController {
  protected service: IService;

  constructor(service: any) {
    this.service = service;
  }

  getAll = async (req: Request, res: Response, next: Function) => {
    const all = await this.service.listAll();
    res.json(all);
  };
  create = async (req: Request, res: Response, next: Function) => {
    const created = await this.service.createElement(req.body);
    res.json(created);
  };
  getOne = async (req: Request, res: Response, next: Function) => {
    const one = await this.service.getElementById(req.params.id);
    res.json(one);
  };
  update = async (req: Request, res: Response, next: Function) => {
    const updated = await this.service.updateElement(req.params.id, req.body);
    res.json(updated);
  };
  delete = async (req: Request, res: Response, next: Function) => {
    const deleted = await this.service.deleteElement(req.params.id);
    res.json(deleted);
  };
}
