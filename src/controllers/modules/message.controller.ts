import IService from "../../interfaces/Services/IService";
import BaseController from "../base.controller";

export default class MessageController extends BaseController {
  constructor(service: IService) {
    super(service);
  }
}
