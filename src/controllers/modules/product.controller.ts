import IService from "../../interfaces/Services/IService";
import BaseController from "../base.controller";

export default class ProductController extends BaseController {
  constructor(service: IService) {
    super(service);
  }
}
