import { Router } from "express";
export const userRouter = (controller: any) => {
  const routes = Router();

  routes.get("/:id", controller.getOne);
  routes.get("/", controller.getAll);
  routes.post("/", controller.create);
  routes.patch("/:id", controller.update);
  routes.delete("/:id", controller.delete);

  routes.post("/login", controller.login);

  return routes;
};

export default userRouter;
