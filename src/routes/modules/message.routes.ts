import { Router as router } from "express";
import IController from "../../interfaces/Controllers/IController";
export const messageRouter = (controller: IController) => {
  const routes = router();

  routes.get("/:id", controller.getOne);
  routes.get("/", controller.getAll);
  routes.post("/", controller.create);
  routes.patch("/:id", controller.update);
  routes.delete("/:id", controller.delete);

  return routes;
};
