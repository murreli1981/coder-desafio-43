import IRepository from "../interfaces/Repositories/IRepository";
import IService from "../interfaces/Services/IService";

export default class BaseService implements IService {
  private readonly repository: IRepository;

  constructor(repository: IRepository) {
    this.repository = repository;
  }
  async getByFilter(filter: {}): Promise<any> {
    try {
      return await this.repository.getByFilter(filter);
    } catch (err) {
      throw new Error("error: " + err);
    }
  }
  async listAll(): Promise<any> {
    try {
      return await this.repository.getAll();
    } catch (err) {
      throw new Error("error: " + err);
    }
  }
  async getElementById(id: String): Promise<any> {
    try {
      return await this.repository.getById(id);
    } catch (err) {
      throw new Error("error: " + err);
    }
  }
  async createElement(element: any): Promise<any> {
    try {
      await this.repository.create(element);
      return { msg: "created", element };
    } catch (err) {
      throw new Error("error: " + err);
    }
  }

  async updateElement(id: String, payload: any): Promise<any> {
    try {
      await this.repository.update(id, payload);
      return { msg: "updated", id, payload };
    } catch (err) {
      throw new Error("error: " + err);
    }
  }
  async deleteElement(id: String): Promise<any> {
    try {
      await this.repository.delete(id);
      return { msg: "deleted", id };
    } catch (err) {
      throw new Error("error: " + err);
    }
  }
}
