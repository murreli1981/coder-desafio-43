import IRepository from "../../interfaces/Repositories/IRepository";
import BaseService from "../base.service";

export default class ProductService extends BaseService {
  constructor(repository: IRepository) {
    super(repository);
  }
}
