# coder-desafio-43

Api Restful - Typescript

## Estructura del proyecto:

```
.
├── README.md
├── nodemon.json
├── package-lock.json
├── package.json
├── src
│   ├── config
│   │   └── globals.ts
│   ├── controllers
│   │   ├── auth
│   │   │   └── auth.controller.ts
│   │   ├── base.controller.ts
│   │   ├── index.ts
│   │   └── modules
│   │       ├── message.controller.ts
│   │       ├── product.controller.ts
│   │       └── user.controller.ts
│   ├── dal
│   │   ├── db
│   │   │   └── connection.ts
│   │   ├── models
│   │   │   ├── index.ts
│   │   │   ├── message.ts
│   │   │   ├── product.ts
│   │   │   └── user.ts
│   │   └── repositories
│   │       ├── base.repository.ts
│   │       ├── index.ts
│   │       └── modules
│   │           ├── message.repository.ts
│   │           ├── product.repository.ts
│   │           └── user.repository.ts
│   ├── index.ts
│   ├── interfaces
│   │   ├── Controllers
│   │   │   └── IController.ts
│   │   ├── Repositories
│   │   │   └── IRepository.ts
│   │   ├── Routes
│   │   │   └── IRoutes.ts
│   │   └── Services
│   │       └── IService.ts
│   ├── routes
│   │   ├── auth
│   │   │   └── auth.routes.ts
│   │   ├── index.ts
│   │   └── modules
│   │       ├── message.routes.ts
│   │       ├── product.routes.ts
│   │       └── user.routes.ts
│   ├── server
│   │   └── server.ts
│   └── services
│       ├── auth
│       │   └── auth.service.ts
│       ├── base.service.ts
│       ├── index.ts
│       └── modules
│           ├── message.service.ts
│           ├── product.service.ts
│           └── user.service.ts
└── tsconfig.json
```

## Inicio del server

el server se ejecuta corriendo `npx start` que se encargará de transpilar ts y correr node

## Paths:

### User:

`[GET] /user` devuelve todos los usuarios.

`[GET] /user/:id ` devuelve un usuario por id.

`[POST] /user` crea un usuario.

`[PATCH] /user/:id` actualiza uno o varios campos del usuario.

`[DELETE] /user/:id` borra un producto.

### Product:

`[GET] /product` devuelve todos los producto.

`[GET] /product/:id ` devuelve un producto por id.

`[POST] /product` crea un producto.

`[PATCH] /product/:id` actualiza uno o varios campos del producto.

`[DELETE] /product/:id` borra un producto.

### Message:

`[GET] /message` devuelve todos los mensajes.

`[GET] /message/:id ` devuelve un mensaje por id.

`[POST] /message` crea un mensaje.

`[PATCH] /message/:id` actualiza uno o varios campos del mensaje.

`[DELETE] /message/:id` borra un mensaje.

### Auth

`[POST] /login` authenticación.

### Documentación

en la colección de postman: `resources/coder-house-backend.postman_collection.json`, carpeta: `coder-desafio-43` están todos los endpoints listados y con datos cargados para probar.

🚨 Todos los endpoints están con el path `localhost:3002` 🚨
